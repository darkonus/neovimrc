"""""""""""""""""""""""""
" My NeoVim Config File "
"                       "
"""""""""""""""""""""""""

" Plug
" -------------------------------------------------- {{{

call plug#begin('~/.nvim/plugged')
" -------------
" --- Filesystem
" -------------
Plug 'scrooloose/nerdtree' ", { 'on':  'NERDTreeToggle' }

" -------------
" --- Utility
" -------------
Plug 'junegunn/fzf'
Plug 'tpope/vim-surround'
Plug 'tomtom/tcomment_vim'
Plug 'Yggdroot/indentLine'
Plug 'easymotion/vim-easymotion'
Plug 'sjl/gundo.vim'
Plug 'ervandew/supertab'

" -------------
" --- Theme
" -------------
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'Chiel92/vim-autoformat'
Plug 'ryanoasis/vim-devicons'
Plug 'morhetz/gruvbox'

" -------------
" --- Autocompletion
" -------------
Plug 'Shougo/deoplete.nvim'

" -------------
" --- Python
" -------------
Plug 'davidhalter/jedi-vim', {'for': 'python'}
Plug 'zchee/deoplete-jedi'

" -------------
" --- Formating
" -------------
Plug 'PotatoesMaster/i3-vim-syntax'

" -------------
" --- Git
" -------------
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'

" -------------
" --- Misc
" -------------
Plug 'neomutt/neomutt.vim'
Plug 'neomake/neomake'

" -------------
" --- Tests
" -------------
" Plug 'marijnh/tern_for_vim', {'for':'javascript'}
" Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
" Plug 'othree/yajs.vim', {'for':'javascript'}
Plug 'wokalski/autocomplete-flow'
Plug 'digitaltoad/vim-pug'
Plug 'jelera/vim-javascript-syntax', {'for':'javascript'}
Plug 'honza/vim-snippets'

Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'

Plug 'junegunn/vim-easy-align'

Plug 'HerringtonDarkholme/yats.vim'

Plug 'prettier/vim-prettier', { 'do': 'yarn install' }

Plug 'mattn/emmet-vim'
Plug 'Raimondi/delimitMate'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'gcmt/wildfire.vim'
call plug#end()
" }}}

" System
" -------------------------------------------------- {{{
syntax on
filetype indent plugin on
colorscheme gruvbox
set background=dark
" Linenumbers
set number
" set relativenumber number
set numberwidth=1
" Foldstuff
set foldmethod=marker
" Highlight tabstops
set list lcs=tab:\|\
" Mouse
set mouse=a
" remove trailing spaces
autocmd BufWritePre * %s/\s\+$//e
" collection
set laststatus=2
set noswapfile
set tabstop=2 shiftwidth=2 expandtab
set virtualedit=block
set conceallevel=0
set wildmenu
set wildmode=full
set autoread
set undofile
set undodir="$HOME/.VIM_UNDO_FILES"
set noshowmode
set hidden
set wrap linebreak nolist
set sidescroll=1
" Direct use of clipboard
" linux:
set clipboard+=unnamedplus
" windows
" set clipboard=unamed
" Remember position in file
autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif

"}}}

" Ubuntu Settings
" -------------------------------------------------- {{{
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'


"  }}}

" Mappings
" -------------------------------------------------- {{{
"leader change
let mapleader = ','

map <leader>tn :tabnew<CR>
map <leader>tc :tabclose<CR>
map <leader>bc :bdelete<CR>
map <leader>w :w<CR>

" no ex mode
nnoremap Q <nop>
" no macros
map q <nop>
noremap <silent> <UP> gk
noremap <silent> <DOWN> gj
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> <HOME> g<HOME>
noremap <silent> <END> g<END>
" single hit commands
nnoremap ; :
" reset hlsearch
nnoremap <silent> <esc> :noh<CR>
" NERDTree
map <silent> <c-\> :NERDTreeToggle<CR>
" Align blocks of text and keep them selected
vmap < <gv
vmap > >gv
" Easy Motion
map <space> <Plug>(easymotion-prefix)
" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
" Use tab to jump between blocks, because it's easier
nnoremap <tab> %
vnoremap <tab> %
" resize panes
" TODO Does not work
noremap <silent> <C-Right> :vertical resize +5<cr>
nnoremap <silent> <C-Left> :vertical resize -5<cr>
nnoremap <silent> <C-Up> :resize +5<cr>
nnoremap <silent> <C-Down> :resize -5<cr>
" AutoFormat
noremap <F3> :Autoformat<CR>
inoremap <F3> <ESC>:Autoformat<CR>a

noremap <c-p> :FZF<CR>

map <c-s> :w<cr>
imap <c-s> <Esc>:w<cr>a



" }}}

" PluginSettings
" -------------------------------------------------- {{{

" NerdTree
autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


" Autocomplete: deoplete
let g:deoplete#enable_at_startup = 1
" nerdtree-git
" let g:NERDTreeIndicatorMapCustom = {
"     \ "Modified"  : "✹",
"     \ "Staged"    : "✚",
"     \ "Untracked" : "✭",
"     \ "Renamed"   : "➜",
"     \ "Unmerged"  : "═",
"     \ "Deleted"   : "✖",
"     \ "Dirty"     : "✗",
"     \ "Clean"     : "✔︎",
"     \ 'Ignored'   : '☒',
"     \ "Unknown"   : "?"
"     \ }
" vim-airline
let g:airline_theme='gruvbox'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#show_buffers = 0


let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>- <Plug>AirlineSelectPrevTab
nmap <leader>= <Plug>AirlineSelectNextTab

" AutoFormat
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0

" Gundo
nmap <leader>u :GundoToggle<CR>
set undodir=~/.vim/undo
set undofile
"maximum number of changes that can be undone
set undolevels=1000000
"maximum number lines to save for undo on a buffer reload
set undoreload=10000000

" Neomake
" call neomake#configure#automake('rw', 200)

"NeoSnippet
" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
let g:neosnippet#snippets_directory = '~/.nvim/plugged/vim-snippets/snippets,~/.config/nvim/vim_snippets'

" }}}

" LanguageSettings
" -------------------------------------------------- {{{
autocmd BufRead,BufNewFile *.launch setlocal filetype=xml

"  Vue
autocmd BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css
" autocmd FileType vue syntax sync fromstart

" Python
au FileType python set tabstop=8 expandtab shiftwidth=4 softtabstop=4

" JS
" autocmd BufRead,BufNewFile *.pug set filetype=javascript


" -------------------------------------------------- }}}

""" Notes
" {{{
"
" }}}
